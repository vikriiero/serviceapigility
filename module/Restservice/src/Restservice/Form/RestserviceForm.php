<?php
namespace Restservice\Form;

use Zend\Form\Form;
// use Zend\Form\Element;
// use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;

class RestserviceForm extends Form
{

    protected $dbAdapter;
    
    // public function __construct($name = null)
    public function __construct(AdapterInterface $dbAdapter)
    {
        
        // we want to ignore the name passed
        // parent::__construct(AdapterInterface $dbAdapter);
        parent::__construct();
        
        $this->setAttribute('class', 'form-horizontal');
        
        $this->dbAdapter = $dbAdapter;
        
        
        $this->add(array(
            'name' => 'id',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'id',
            )
        ));
        
        $this->add(array(
            'name' => 'fullname',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'fullname',
            )
        ));
        
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'username',
            )
        ));
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'email',
            )
        ));
        
        $this->add(array(
            'name' => 'password',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'password',
            )
        ));
        
        $this->add(array(
            'name' => 'submit_village',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Submit',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            )
            
        ));
    }

    public function getListProvince()
    {
        $dbAdapter = $this->dbAdapter;
        $sql = 'SELECT province_id,province_name from province';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        
        $selectData = array();
        
        foreach ($result as $res) {
			$selectData[$res['province_id']] = $res['province_name'];
		}
		return $selectData;
	}
	
}

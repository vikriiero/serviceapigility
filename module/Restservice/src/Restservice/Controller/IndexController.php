<?php
namespace Restservice\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Restservice\Model\Restservice;
use Restservice\Form\RestserviceForm;
// use Restservice\Model\RestserviceTable;
// use Zend\Db\Sql\Select;
// use Zend\View\Model\ViewModel;
// use Zend\Http\Response;

class IndexController extends AbstractRestfulController
{

    protected $restserviceTable;

    protected function methodNotAllowed()
    {
        $this->response->setStatusCode(405);
        throw new \Exception('Method Not Allowed Gan');
    }

    public function get($id)
    {
        $village = $this->getRestserviceTable()->getRestservice($id);
        return new JsonModel(array(
            'data' => $village
        ));
    }

    public function getList()
    {
        $dataList = $this->getRestserviceTable()->getList();
        
        return new JsonModel(array(
            'data' => $dataList->toArray()
        ));
    }

    public function create($data)
    {
        
//         print_r($data); die; 
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $formRestservice = new RestserviceForm($dbAdapter);
        
        $id = '';
        $role = new Restservice();
        $formRestservice->setInputFilter($role->getInputFilter());
        $formRestservice->setData($data);
        
        if ($formRestservice->isValid()) {
            $role->exchangeArray($formRestservice->getData());
            $id = $this->getRestserviceTable()->saveRestservice($role);
            return new JsonModel(array(
                'data' => 'success insert data'
            ));
        } else {
            
            $err = $formRestservice->getMessages();
            return new JsonModel(array(
                'data' => 'canot insert data'
            ));
        }
    }
    
    
    public function update($id,$data){

        
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
         $village = $this->getRestserviceTable()->getRestservice($id);

         $data['id'] = $village->id;
         
         
          $village->id = $village->id;
         
        $formVillage = new RestserviceForm($dbAdapter);
        $formVillage->bind($village);
        $formVillage->setInputFilter($formVillage->getInputFilter());
        $formVillage->setData($data);  
        
        
       	if ($formVillage->isValid()) {
       	    
       	 	try {
  	    $res = 	$this->getRestserviceTable()->updateRestservice($data);

        		
        		}catch(\Exception $err){
        		    
        		    die('asds');
        		    return new JsonModel(array(
        		    		'data' => $err->getMessage()
        		    ));
        		    die('sdf');
        		}
        	}else{
        	    die;
        	    return new JsonModel(array(
        	    		'data' => $err->getMessage()
        	    ));
        	    
        	    
        	}
        	
        	
        	if($res){
        	    
        	    return new JsonModel(array(
        	    		'data' => 'success update data'
        	    ));
        	}else{
        	    return new JsonModel(array(
        	    		'data' => 'failed update data'
        	    ));
        	}
        	
        	
        	
        	
        	
        	
        	die;
    }
    
    
    public function delete($id){
        $del  = $this->getRestserviceTable()->deleteRestservice($id);
        if($del){
            
            return new JsonModel(array(
            		'data' => 'success delete data'
            ));
            
        }else {
            return new JsonModel(array(
            		'data' => 'failed delete data'
            ));
        }
        
        
    }
    public function updateee($id,$data){
        
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $formRestservice = new RestserviceForm($dbAdapter);

        $village  = $this->getRestserviceTable()->getRestservice($id);
        
        $form = new Restservice();
        $form->bind($village);
        $form->setInputFilter($village->getInputFilter());
       $form->setData($data);
       
        if ($form->isValid()) { 
        	$form->exchangeArray($formRestservice->getData());
        	$id = $this->getRestserviceTable()->updateRestservice($form->getData());
        	return new JsonModel(array(
        			'dataa' => $this->get($id)
        	));
        } else {
        
        	$err = $formRestservice->getMessages();

        	return new JsonModel(array(
        			'dataa' => $err
        	));
        }
    }
    
    
    function getRestserviceTable()
    {
        if (! $this->restserviceTable) {
            $sm = $this->getServiceLocator();
            $this->restserviceTable = $sm->get('Restservice\Model\RestserviceTable');
        }
        return $this->restserviceTable;
    }
}
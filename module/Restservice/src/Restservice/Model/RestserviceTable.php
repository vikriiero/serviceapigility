<?php
namespace Restservice\Model;

// use Zend\Db\TableGateway\TableGateway;
// use Restservice\Form\Filter\RestserviceFilter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;



class RestserviceTable extends AbstractTableGateway
{

    protected $tableGateway;

    protected $table = 'user';
    
    // public function __construct(TableGateway $tableGateway)
    // {
    // $this->tableGateway = $tableGateway;
    // }
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Restservice());
        $this->initialize();
    }

    public function saveRestservice(Restservice $village)
    {  
        try {

            $data = array(
            	'id' => $village->id,
                'fullname' => $village->fullname,
                'username' => $village->username,
                'email' => $village->email,
                'password' => $village->password,
            );
            
         return  $this->insert($data);

        }catch (\Exception $err){
            echo $err->getMessage();die;    
            return false;
        }
    }

    public function updateRestservice($data)
    {
    
          $datas = array(
            	'id' => $data->id,
                'fullname' => $data->fullname,
                'username' => $data->username,
                'email' => $data->email,
                'password' => $data->password,
            );

        try {

            $this->update($datas, array('id' =>$data['id'] ));

             return true;
        }catch (\Exception $err){
            
           return false;
        }
        
    }

    
    public function getRestservice($id)
    {
        $id = (int) $id;
        $rowset = $this->select(array(
            'id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getList(){
        $resultSet = $this->select();
        return $resultSet;
    }

    public function fetchAll(Select $select = null)
    {
        if (null === $select)
            $select = new Select();
        $select->from($this->table);
        $select->join('province','province.province_id = village.province_id');
        $select->join('regency','regency.regency_id = village.regency_id');
        $select->join('district','district.district_id = village.district_id');
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
    }
    
    public function deleteRestservice($id) { 
        try{
            if($this->delete(array('id' => $id))){
                return true;
            }
            return false;
            
        }catch (\Exception $err){
            return false;
        }
    	
    }
    
}
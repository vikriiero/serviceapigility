<?php

namespace Restservice\Model;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Restservice implements InputFilterAwareInterface {

    
        
    public $id;
    public $fullname;
    public  $username;
    public  $email;
    public  $password;
    
    
    protected $inputFilter;
    
    public function setPassword($clear_password)
    {
        $this->password = md5($clear_password);
    }
    

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
     
    
    
    
    public function exchangeArray($data){ 
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->fullname     = (!empty($data['fullname'])) ? $data['fullname'] : null;
    	$this->username     = (!empty($data['username'])) ? $data['username'] : null;
    	$this->email     = (!empty($data['email'])) ? $data['email'] : null;
    	
    	if (isset($data["password"]));
    	{
    	    $this->setPassword($data["password"]);
    	}
    	
    }
    
    
	public function getInputFilter(){
	    if (!$this->inputFilter) {
    	    $inputFilter = new InputFilter();
    	    
    	    $inputFilter->add(array(
    	    		'name'     => 'fullname',
    	    		'required' => true,
    	    		'filters'  => array(
    	    				array('name' => 'StringTrim'),
    	    		),
    	    ));
    	    $this->inputFilter = $inputFilter;
    	    }
    	   return $this->inputFilter;
	}

    
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
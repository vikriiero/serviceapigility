<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Restservice\Controller\Index' => 'Restservice\Controller\IndexController'
        )
    ),
    'router' => array(
        'routes' => array(
            'restservice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/restservice[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    )
                    ,
                    'defaults' => array(
                        'controller' => 'Restservice\Controller\Index'
                    )
                )
            )
        )
    ),
    
    'view_manager' => array(
        
        'strategies' => array(
            'ViewJsonStrategy'
        )
    )
    
)
;

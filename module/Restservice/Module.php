<?php


namespace Restservice;


use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
// use Zend\Mvc\Application;
// use Zend\Mvc\Controller\Plugin\Redirect;

// use Zend\Authentication\Adapter\DbTable as DbAuthAdapter;
// use Zend\Session\Container;
// use Zend\Authentication\AuthenticationService;
// use MyLib\Utility\Acl;
// use MyLib\Utility\Userr;
// use Zend\Mvc\Controller\Plugin\AbstractPlugin;


use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Restservice\Model\RestserviceTable;
// use Zend\Db\TableGateway\TableGateway;
// use Zend\Db\ResultSet\ResultSet;
// use Login\Model\Restservice;
// use Restservice\Form\RestserviceForm;
// use Restservice\Form\Filter\RestserviceFilter;





class Module implements AutoloaderProviderInterface, ConfigProviderInterface
    {
        
        public function onBootstrap(MvcEvent $e)
        {
        	$eventManager = $e->getApplication()->getEventManager();
        	
        	
        	
        	$moduleRouteListener = new ModuleRouteListener();
        	$moduleRouteListener->attach($eventManager);
        	$eventManager->attach(MvcEvent::EVENT_DISPATCH, array(
        			$this,
        			'beforeDispatch'
        	), 100);
        }
        
        function beforeDispatch(MvcEvent $event)
        {
        
            
        	$request = $event->getRequest();
        	
        	
        	$response = $event->getResponse();
        	
        	 
        	//$target = $event->getTarget();
        	
        	error_log(print_r($request, 1), 3, "/tmp/inthekost.log");
        	 
        	
        }
        
	public function getAutoloaderConfig()
	{ 
		return array(
				'Zend\Loader\ClassMapAutoloader' => array(
						__DIR__ . '/autoload_classmap.php',
				),
				'Zend\Loader\StandardAutoloader' => array(
						'namespaces' => array(
								__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
						),
				),
		);
	}

	public function getConfig()
	{

		return include __DIR__ . '/config/module.config.php';
	}
	
	public function getServiceConfig()
	{ 
	    	 
	    $return = array(
	    		'Zend\Loader\ClassMapAutoloader' => array(
	    				__DIR__ . '/autoload_classmap.php'
	    		),
	    		'Zend\Loader\StandardAutoloader' => array(
	    				'namespaces' => array(
	    						__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
	    						'DataTables' => __DIR__ . '/../../vendor/DataTables',
	    				)
	    		)
	    );
	    
	    /*
	     *     
    
// 	return array(
// 			'factories' => array(
// 					'Album\Model\AlbumTable' =>  function($sm) {
// 						$tableGateway = $sm->get('AlbumTableGateway');
// 						$table = new AlbumTable($tableGateway);
// 						return $table;
// 					},
// 					'AlbumTableGateway' => function ($sm) {
// 						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
// 						$resultSetPrototype = new ResultSet();
// 						$resultSetPrototype->setArrayObjectPrototype(new Album());
// 						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
// 					},
// 			),
// 	);

	     */
	    
	    return array(
	    		'factories' => array(
	    				'Restservice\Model\RestserviceTable' =>  function($sm) {
	    					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
	    					$table = new RestserviceTable($dbAdapter);
	    					return $table;
	    				},
	    		),
	    );
	    
	    
// 		return array(
// 				'factories' => array(
// 						'Restservice\Model\RestserviceTable' =>  function($sm) {
// 						    $tableGateway = $sm->get('RestserviceTableGateway'); 
// 							$table = new RestserviceTable($tableGateway); 
// 							return $table;
// 						},
// 						'RestserviceTableGateway'=> function ($sm){
//                             $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter'); 
//                             $resultSetPrototype = new ResultSet();
//                             $resultSetPrototype->setArrayObjectPrototype(new RestserviceFilter());
//      						return new TableGateway('role', $dbAdapter, null, $resultSetPrototype);
// 						},
// 				),
// 				'aliases' => array(
// 						'zfdb_adapter' => 'Zend\Db\Adapter\Adapter',
// 				),
// 		);

	}
	

}
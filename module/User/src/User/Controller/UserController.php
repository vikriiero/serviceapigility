<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/User for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use User\Form\UserForm;
use SanAuth\Model\User;
use Zend\Form\Annotation\Validator;


class UserController extends AbstractActionController
{
    protected $userTable;
    public function indexAction()
    {
         $form = new UserForm(); 
         $request = $this->getRequest(); 

    if($request->isPost()) 
    { 
        $user = new User(); 
        
        $formValidator = new Validator(); 
        { 
            $form->setInputFilter($formValidator->getInputFilter()); 
            $form->setData($request->getPost()); 
        } 
         
        if($form->isValid()){ 
        { 
            $user->exchangeArray($form->getData()); 
        } 
        }
    } 
    
    return ['form' => $form];
    } 
    
    
    
    public function addAction()
    {
        
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /user/user/foo
        return array();
    }
    
    public function getUserTable()
    {
        if(!$this->userTable){
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }
}

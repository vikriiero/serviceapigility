<?php
namespace MusicRestTest\Controller;

use MusicRestTest\Bootstrp;
use MusicRest\Controller\MusicRestController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use PHPUnit_Framework_TestCase;


// use Zend\Mvc\Controller\AbstractActionController;
// use Zend\View\Model\ViewModel;

/**
 * MusicRestControllerTest
 *
 * @author
 *
 * @version
 *
 */
class MusicRestControllerTest extends PHPUnit_Framework_TestCase
{
    
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $even;
    
    public function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->Controller = new MusicRestController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->even       = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig   = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->even->setRouter($router);
        $this->even->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->even);
        $this->controller->setServiceLocator($serviceManager);
    }
    public function testGetListCanBeAccessed()
    {
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getRequest();
    
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testGetCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
    
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
    
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testCreateCanBeAccessed()
    {
        $this->request->setMethod('post');
        $this->request->getPost()->set('artist', 'foo');
        $this->request->getPost()->set('title', 'bar');
    
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
    
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testUpdateCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('put');
    
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
    
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('delete');
    
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
    
        $this->assertEquals(200, $response->getStatusCode());
    }

    
}
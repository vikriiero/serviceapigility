<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'MusicRest\Controller\MusicRest' => 'MusicRest\Controller\MusicRestController',
            'MusicRest\Controller\MusicRestTest' => 'MusicRest\Controller\MusicRestControllerTest'
        ),
    ),
    'router' => array(
        'routes' => array(
            'music-rest' => array(
                'type'    => 'segment',
                    'options' => array(
                            'route'    => '/music-rest[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'MusicRest\Controller\MusicRest',
                            ),
                        ),
                    ),
                ),
            ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);

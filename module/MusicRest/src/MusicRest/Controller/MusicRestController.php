<?php
namespace MusicRest\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Application\Model\Register;
use Application\Form\RegisterForm;
use Application\Form\RegisterFilter;
use Application\Model\RegisterTable;
use Zend\View\Model\JsonModel;
use Zend\View\Helper\ViewModel;


class MusicRestController extends AbstractRestfulController
{
    protected $registerTable;
    
    public function getList()
    {
        $results = $this->getRegisterTable()->fetchAll();
        $data = array();
        foreach($results as $result) {
            $data[] = $result;
            //         print_r($data);
            //         die;
        }
        return new JsonModel($data);
    }
    
    public function get($id)
    {
        $register = $this->getRegisterTable()->getRegister($id);
            foreach($register as $result) {
            $data[] = $result;
        }
    
        return new JsonModel($data);
        return array("data" => $register);
    }
    
    
    public function getRegisterTable()
    {
        if(!$this->registerTable) {
            $sm = $this->getServiceLocator();
            
            $this->registerTable = $sm->get('Application\Model\RegisterTable');
            
        }
        
        return $this->registerTable;
    }
    
     public function testGetRegisterTableReturnsAnInstanceOfRegisterTable()
    {
        $this->assertInstanceOf('Application\Model\RegisterTable', $this->controller->getRegisterTable());
    }
    
    
    
//     
    public function create($data)
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        $data=array();
        
        // Validasi username
        $user = $this->getRegisterTable()->getRegisterByName($postData['username']);
        if($user){
            $data[]=array('msg'=>'Username has been exist.');
            return new JsonModel($data);
        }
        
        //Validasi email
        $email = $this->getRegisterTable()->getRegisterByEmail($postData['email']);
        if($email){
            $data[]=array('msg'=>'Email has been exist.');
            return new JsonModel($data);
        }
        
        $post = $this->request->getPost();
        $form = new RegisterForm();
        $inputFilter = new RegisterFilter();
        $form->setInputFilter($inputFilter);
        $form->setData($post);
        //         print_r($post);
        
        
        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error'=>true,
                'form'=>$form,
            ));
        
        
            $model->setTemplate('application/register/register');
            return $model;
             
        }
        $this->createRegister($form->getData());
        return $this->redirect()->toUrl(array(
            'action'=>'music-rest',
        ));
        
    }
    
    public function update($id, $data)
    {
        $data['id'] = $id;
        $register = $this->getRegisterTable()->getRegister($id);
        $form = new RegisterForm();
        $form->bind($register);
        $form->setInputFilter($register->getInputFilter());
        $form->setData($data);
        if ($form->isValid()) {
            $id = $this->getRegisterTable()->saveRegister($form->getData());
        }
        
        return new JsonModel(array(
            'data' => $this->get($id),
        ));
        
    }
    
    public function delete($id)
    {
        $this->getRegisterTable()->deleteRegister($id);
        
        return new JsonModel(array(
            'data' => 'deleted',
        ));
        
    }
    protected function createRegister(array $data)
    {
        $sm = $this->getServiceLocator();
        $dbAdapter = $sm->get('zend\Db\Adapter\Adapter');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Register);
        $tableGateway = new \Zend\Db\TableGateway\TableGateway('user', $dbAdapter, null, $resultSetPrototype);
        $register = new Register();
        $register->exchangeArray($data);
        $registerTable = new RegisterTable($tableGateway);
        $registerTable->saveRegister($register);
        return true;
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /musicRest/music-rest/foo
        return array();
    }
}

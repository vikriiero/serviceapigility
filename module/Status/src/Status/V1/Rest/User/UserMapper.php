<?php
namespace  Status\V1\Rest\User;

use Zend\Db\Sql\Select;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;

class UserMapper
{
    protected $adapter;
    
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapte = $adapter;
    }
    public function fetchAll()
    {
        $select = new Select('user');
        $paginatorAdapter = new DbSelect($select, $this->adapter);
        $collection = new UserCollection($paginatorAdapter);
        
        return $collection;
    }
}
<?php
namespace Status\V1\Rest\User;

use Zend\Paginator\Paginator;

class UserCollection extends Paginator
{
    public function __construct($paginatorAdapter)
    {
        $this->adapter = $paginatorAdapter;
    }
}

<?php
namespace Status\V1\Rest\User;

class UserResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Status\V1\Rest\User\UserMapper');
        return new UserResource($mapper);
    }
}

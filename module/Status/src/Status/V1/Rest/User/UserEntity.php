<?php
namespace Status\V1\Rest\User;

class UserEntity
{
    public $id;
    public $fullname;
    public $username;
    public $email;
    
    public function getArrayCopy()
    {
        return array(
            'id' => $this->id,
            'fullname' => $this->fullname,
            'username' => $this->username,
            'email'    => $this->email,
        );
    }
    public function exchangeArray(array $array)
    {
        $this->id = $array['id'];
        $this->fullname = $array['fullname'];
        $this->username = $array['username'];
        $this->email    = $array['email'];
    }
    
}

<?php
namespace Status\V1\Rest\Message;

use Zend\Paginator\Paginator;

class MessageCollection extends Paginator
{
    public function __construct($paginatorAdadter)
    {
        $this->adapter = $paginatorAdadter;
    }
}

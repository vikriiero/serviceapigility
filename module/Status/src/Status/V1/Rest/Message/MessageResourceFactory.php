<?php
namespace Status\V1\Rest\Message;

class MessageResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Music\V1\Rest\Message\MessageMapper');
        return new MessageResource($mapper);
    }
}

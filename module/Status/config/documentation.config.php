<?php
return array(
    'Status\\V1\\Rpc\\Ping\\Controller' => array(
        'GET' => array(
            'description' => 'Ping the API for acknowledgement',
            'request' => null,
            'response' => '{
   "ack": "Acknowledge the request with a timestamp"
}',
        ),
        'description' => 'Ping th API',
    ),
    'Status\\V1\\Rest\\Status\\Controller' => array(
        'description' => 'Status API',
    ),
);

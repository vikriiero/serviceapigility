<?php


namespace Restserviceclient;


use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Restserviceclient\Model\RestserviceclientTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Login\Model\Restserviceclient;
use Restserviceclient\Form\RestserviceclientForm;
use Restserviceclient\Form\Filter\RestserviceclientFilter;




class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
	public function getAutoloaderConfig()
	{ 
		return array(
				'Zend\Loader\ClassMapAutoloader' => array(
						__DIR__ . '/autoload_classmap.php',
				),
				'Zend\Loader\StandardAutoloader' => array(
						'namespaces' => array(
								__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
						),
				),
		);
	}

	public function getConfig()
	{

		return include __DIR__ . '/config/module.config.php';
	}
	
	public function getServiceConfig()
	{ 
	 
	    $return = array(
	    		'Zend\Loader\ClassMapAutoloader' => array(
	    				__DIR__ . '/autoload_classmap.php'
	    		),
	    		'Zend\Loader\StandardAutoloader' => array(
	    				'namespaces' => array(
	    						__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
	    						'DataTables' => __DIR__ . '/../../vendor/DataTables',
	    				)
	    		)
	    );
	    
	    /*
	     *     
    
// 	return array(
// 			'factories' => array(
// 					'Album\Model\AlbumTable' =>  function($sm) {
// 						$tableGateway = $sm->get('AlbumTableGateway');
// 						$table = new AlbumTable($tableGateway);
// 						return $table;
// 					},
// 					'AlbumTableGateway' => function ($sm) {
// 						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
// 						$resultSetPrototype = new ResultSet();
// 						$resultSetPrototype->setArrayObjectPrototype(new Album());
// 						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
// 					},
// 			),
// 	);

	     */
	    
	    return array(
	    		'factories' => array(
	    				'Restserviceclient\Model\RestserviceclientTable' =>  function($sm) {
	    					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
	    					$table = new RestserviceclientTable($dbAdapter);
	    					return $table;
	    				},
	    		),
	    );
	    
	    
// 		return array(
// 				'factories' => array(
// 						'Restserviceclient\Model\RestserviceclientTable' =>  function($sm) {
// 						    $tableGateway = $sm->get('RestserviceclientTableGateway'); 
// 							$table = new RestserviceclientTable($tableGateway); 
// 							return $table;
// 						},
// 						'RestserviceclientTableGateway'=> function ($sm){
//                             $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter'); 
//                             $resultSetPrototype = new ResultSet();
//                             $resultSetPrototype->setArrayObjectPrototype(new RestserviceclientFilter());
//      						return new TableGateway('role', $dbAdapter, null, $resultSetPrototype);
// 						},
// 				),
// 				'aliases' => array(
// 						'zfdb_adapter' => 'Zend\Db\Adapter\Adapter',
// 				),
// 		);

	}
	

}
<?php  


return array(
    'controllers' => array(
        'invokables' => array(
            'Restserviceclient\Controller\Index' => 'Restserviceclient\Controller\IndexController',
            'Restserviceclient\Controller\Login' => 'Restserviceclient\Controller\LoginController'
        )
    ),
    'router' => array(
        'routes' => array(
            'restserviceclient' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/restserviceclient[/:action][/:id][/page/:page][/order_by/:order_by][/:order]',
                    'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                    'constraints' => array(
                           'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                        'page' => '[0-9]+',
                        'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'order' => 'ASC|DESC',
                 
                    ),
                    'defaults' => array(
                        'controller' => 'Restserviceclient\Controller\Index',
                        'action' => 'index'
                    ),
                ),
            ),
            'restservicelogin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/restservicelogin[/:action]',
                    'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Restserviceclient\Controller\Login',
                        'action'     => 'login',
                        ),
                    ),
                ),
        ),
    ),
    
    'view_manager' => array(
    		'display_not_found_reason' => true,
    		'display_exceptions' => true,
    		'doctype' => 'HTML5',
    		'not_found_template' => 'error/404',
    		'exception_template' => 'error/index',
    		'template_map' => array(
    		//		'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
    				'restserviceclient/index/index' => __DIR__ . '/../view/restserviceclient/index/index.phtml',
    				'error/404' => __DIR__ . '/../view/error/404.phtml',
    				'error/index' => __DIR__ . '/../view/error/index.phtml',
    		          'paginator-slide' => __DIR__ . '/../view/layout/slidePaginator.phtml',
    		    
    		),
    		'template_path_stack' => array(
    			'restserviceclient' => __DIR__ . '/../view',
    		   
    		),
            'strategies' => array(
        		'ViewJsonStrategy',
            ),
        
    ),
    
    
      
    
);

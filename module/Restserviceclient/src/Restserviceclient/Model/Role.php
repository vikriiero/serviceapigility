<?php

namespace Role\Model;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Role implements InputFilterAwareInterface {

    
        
    public $role_name;
    public $role_id;
    
    protected $inputFilter;
    

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
     
    
    
    
    public function exchangeArray($data)
    {
    	$this->role_name     = (!empty($data['role_name'])) ? $data['role_name'] : null;
    	$this->role_id     = (!empty($data['role_id'])) ? $data['role_id'] : null;
    	$this->role_status     = (!empty($data['role_status'])) ? $data['role_status'] : null;
    	
    }
    
    
	public function getInputFilter(){
	    //die('s'    );
	    if (!$this->inputFilter) {
	        
	      //  die('sd');
	    $inputFilter = new InputFilter();
	    
	    $inputFilter->add(array(
	    		'name'     => 'role_name',
	    		'required' => true,
	    		'filters'  => array(
	    				array('name' => 'StringTrim'),
	    		),
	    ));
	    $this->inputFilter = $inputFilter;
	    }
	   
	   return $this->inputFilter;
	}

    
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
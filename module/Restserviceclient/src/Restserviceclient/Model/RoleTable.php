<?php
namespace Role\Model;

// use Zend\Db\TableGateway\TableGateway;
// use Role\Form\Filter\RoleFilter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;

class RoleTable extends AbstractTableGateway
{

    protected $tableGateway;

    protected $table = 'role';
    
    // public function __construct(TableGateway $tableGateway)
    // {
    // $this->tableGateway = $tableGateway;
    // }
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Role());
        $this->initialize();
    }

    public function saveRole(Role $role)
    { 
        $data = array(
            'role_name' => $role->role_name
        );
        return $this->insert($data);
    }

    public function updateRole(Role $role)
    {
        $data = array(
            'role_id' => $role->role_id,
            'role_name' => $role->role_name
        );
        try {

print_r($data); die; 
            $this->update($data, array(
            		'role_id' => $role->role_id
            ));
            
        }catch (\Exception $err){
            echo $err->getMessage();
            die;
        }
        
    }

    public function getRole($id)
    {
        $id = (int) $id;
        $rowset = $this->select(array(
            'role_id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchAll(Select $select = null)
    {
        if (null === $select)
            $select = new Select();
        $select->from($this->table);
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
    }
    
    public function deleteRole($id) { 
    	$this->delete(array('role_id' => $id));
    }
    
}
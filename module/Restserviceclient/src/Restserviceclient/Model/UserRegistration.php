<?php
namespace Restcerviceclient\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserRegistration implements InputFilterAwareInterface
{
    public $id;
    public $fullname;
    public $username;
    public $email;
    public $password;
    protected $inputFilter;
    
    public function setPassword($clear_password)
    {
        $this->setPassword = md5($clear_password);
    }
    public function exchangeArrayData($data)
    {
        $this->id       = (isset($data['id'])) ? $data['id'] :null;
        $this->fullname = (isset($data['fullname'])) ? $data['fullname'] :null;
        $this->username = (isset($data['username'])) ? $data['username'] :null;
        $this->email    = (isset($data['email'])) ? $data['email']  :null;
        
        if (isset($data['password']))
        {
            $this->setPassword($data['password']);
        }
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            
            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
    
            $inputFilter->add(array(
                'name'     => 'fullname',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));
    
            $inputFilter->add(array(
                'name'     => 'username',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));
            
            $inputFilter->add(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators'=>array(
                    array(
                        'name'=>'EmailAddress',
                        'options'=>array(
                            'messages'=>array(
                                \Zend\Validator\EmailAddress::INVALID_FORMAT=>'Email address format is invalid'
                            )
                        )
                    )
                )
            ));
            
            $inputFilter->add(array(
                'name'     => 'password',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 6,
                            'max'      => 14,
                        ),
                    ),
                ),
            ));
//             $inputFilter->add(array(
//                 'name'     => 'conf_pass',
//                 'required' => true,
//                 'filters'  => array(
//                     array('name' => 'StripTags'),
//                     array('name' => 'StringTrim'),
//                 ),
//                 'validators' => array(
//                     array(
//                         'name'    => 'StringLength',
//                         'options' => array(
//                             'encoding' => 'UTF-8',
//                             'min'      => 6,
//                             'max'      => 14,
//                         ),
//                     ),
//                 ),
//             ));
            
    
            $this->inputFilter = $inputFilter;
        }
    
        return $this->inputFilter;
    }
}
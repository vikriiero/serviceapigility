<?php
namespace Restserviceclient\Controller;

use Zend\Mvc\Controller\AbstractActionController;

use Zend\View\Model\ViewModel;
use Restserviceclient\Form\LogForm;
use Zend\Http\Client;


class LoginController extends AbstractActionController
{
    
    public function logAction()
    {
        
        $form = new LogForm();
        $viewModel = new ViewModel(array(
            'form' => $form));
        return $viewModel;
    }
    
    public function loginAction()
    {
    
        
        $request = $this->getRequest();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
    
        $request->setUri('http://localhost:1946/restservicelog');
        $request->setMethod('POST');
        $request->getPost();
        //                 array(
        //                     'id' => '124',
        //     					'fullname'=>'adam',
        //     					'username'=>'adam',
        //     					'email'=>'adam@gmail.com',
        //     			     	'password'=>'admin',
        //                     )
        //                 ));
    
        $client = new Client();
        $response = $client->dispatch($request);
        echo $response->getBody();
        die;
    }
}
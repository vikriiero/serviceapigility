<?php
namespace Restserviceclient\Controller;

use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Stdlib\Parameters;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Restserviceclient\Form\RegistrationForm;
// use Restcerviceclient\Form\LoginForm;

class IndexController extends AbstractActionController
{

    protected $roleTable;
    
    
    public function RegistrationAction()
    {
           $form = new RegistrationForm();
           $viewModel = new ViewModel(array(
               'form' => $form
           ));
           
           return $viewModel;
           
    }
    
    
    public function addAction()
    {
        return new ViewModel();
    }

    public function indexAction()
    {
        
        $request = $this->getRequest();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        
        $request->setUri('http://localhost:1945/restservice');
        $request->setMethod('POST');
        $request->getPost();
        //                 array(
        //                     'id' => '124',
        //     					'fullname'=>'adam',
        //     					'username'=>'adam',
        //     					'email'=>'adam@gmail.com',
        //     			     	'password'=>'admin',
        //                     )
        //                 ));
        
        $client = new Client();
        $response = $client->dispatch($request);
        echo $response->getBody();
    }
    
    public function getAction(){
        
        try{
        
        	$request = new Request();
        	$request->getHeaders()->addHeaders(array(
        			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        	));
        
        	$request->setUri('http://localhost:1945/restservice/98');
        	/// $request->setMethod('POST');
        	//$request->setPost(new Parameters(array('id' => 1)));
        	$client = new Client();
        	$response = $client->dispatch($request);
        	echo $response->getBody();
        
        }catch (\Exception $err){
        	echo $err->getMessage();
        }
        die;
    }
    

    public function getlistAction(){
        
        try{
        
            $request = new Request();
            $request->getHeaders()->addHeaders(array(
                    'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
            ));
        
            $request->setUri('http://localhost:1945/restservice');
            /// $request->setMethod('POST');
            //$request->setPost(new Parameters(array('id' => 1)));
            $client = new Client();
            $response = $client->dispatch($request);


            echo $response->getBody();
        
        }catch (\Exception $err){
            echo $err->getMessage();
        }
        die;
    }
    
    

    public function createAction()
    {
         
            $request = $this->getRequest();
            $request->getHeaders()->addHeaders(array(
                    'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
            ));
        
            $request->setUri('http://localhost:1945/restservice');
            $request->setMethod('POST');
            $request->getPost();
//                 array(
//                     'id' => '124',
//     					'fullname'=>'adam',
//     					'username'=>'adam',
//     					'email'=>'adam@gmail.com',
//     			     	'password'=>'admin',
//                     )
//                 ));

            $client = new Client();
            $response = $client->dispatch($request);
            echo $response->getBody();
        die;
    }
    
    public function loginAction()
    {
        
        
        $request = $this->getRequest();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
    
        $request->setUri('http://localhost:1946/restservicelog');
        $request->setMethod('POST');
        $request->getPost();
        //                 array(
        //                     'id' => '124',
        //     					'fullname'=>'adam',
        //     					'username'=>'adam',
        //     					'email'=>'adam@gmail.com',
        //     			     	'password'=>'admin',
        //                     )
        //                 ));
    
        $client = new Client();
        $response = $client->dispatch($request);
        echo $response->getBody();
        die;
    }
    
    
    public function updateAction()
    {
    	 
    	$request = new Request();
    	$request->getHeaders()->addHeaders(array(
    			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
    	));
    
    	
    	$request->setUri('http://localhost:1945/restservice/97');
        $request->setMethod('PUT');
    	$request->setPost(new Parameters(
    			array(
    					'id' => '102',
    					'fullname'=>'adamm',
    					'username'=>'adam',
    					'email'=>'adam@gmail.com',
    			     	'password'=>'admin'
    			    
    			)
    	));
    
    	
    	$client = new Client();
    	//print_r($request); die;
    	$response = $client->dispatch($request);
    	echo $response->getBody();
    	die;
    }
    
    public function deleteAction(){

        $request = new Request();
        $request->getHeaders()->addHeaders(array(
        		'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        $request->setUri('http://localhost:1945/restservice/0');
        $request->setMethod('DELETE');
        $client = new Client();
        $response = $client->dispatch($request);
        echo $response->getBody();
        die;
        
    }
    
//     public function logAction()
//     {
//         die('dede');
//         $form = new LoginForm();
//         $viewModel = new ViewModel(array(
//             'form' => $form));
//         return $viewModel;
//     }
       
}
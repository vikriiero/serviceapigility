<?php

namespace Role\Form;

use Zend\Form\Form;
// use Zend\Form\Element;

class RoleForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct();
        
		$this->setAttribute('class', 'form-horizontal');
		
		$this->add(array(
				'name' => 'role_name',
				'type' => 'Text',
		    'attributes' => array(
		    		'class' => 'form-control',
		             'placeholder'=>'Enter role name'
		    		
		    ),
		    
		));
		
		$this->add(array(
				'type' => 'Zend\Form\Element\Select',
				'name' => 'role_status',
				'options' => array(
						'empty_option' => 'Pilih Status',
						'value_options' => array(
								'Active' => 'Active',
								'Inactive' => 'Inactive',
						)
				),
				'attributes' => array(
						'class' => 'form-control'
				)
		));
		
		$this->add(array(
				'name' => 'submit',
				'type' => 'Submit',
				'attributes' => array(
						'value' => 'Go',
				    'class'=>'btn btn-primary',
						
				),
		));
	}
	
	
}

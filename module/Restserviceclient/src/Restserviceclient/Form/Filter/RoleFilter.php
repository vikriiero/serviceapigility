<?php

namespace Role\Form\Filter;
use Zend\InputFilter\InputFilter;
class RoleFilter extends InputFilter {

	public function __construct(){

		$isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
		$invalidEmail = \Zend\Validator\EmailAddress::INVALID_FORMAT;

		$this->add(array(
				'name' => 'role_name',
				'required' => true,
				'filters' => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				),
				'validators' => array(
						array(
								'name' => 'NotEmpty',
								'options' => array(
										'messages' => array(
												$isEmpty => 'Role can not be empty.'
										)
								),
								'break_chain_on_failure' => true
						),
					
				),
		));

	}

public function exchangeArray($data)
    {
    	$this->role_name     = (!empty($data['role_name'])) ? $data['role_name'] : null;
    }
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
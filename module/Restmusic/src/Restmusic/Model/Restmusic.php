<?php

namespace Restmusic\Model;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class Restmusic implements InputFilterAwareInterface {

    
        
    public $id;
    public $fullname;
    public $username;
    public $email;
    public $password;
    
    
    protected $inputFilter;
    

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
    	throw new \Exception("Not used");
    }
     
    
    
    
    public function exchangeArray($data){
        $this->id           = (!empty($data['id'])) ? $data['id'] : null;
        $this->fullname     = (!empty($data['fullname'])) ? $data['fullname'] : null;
    	$this->username     = (!empty($data['username'])) ? $data['username'] : null;
    	$this->email        = (!empty($data['email'])) ? $data['email'] : null;
    	$this->password     = (!empty($data['password'])) ? $data['password'] : null;
    }
    
    
	public function getInputFilter(){
	    if (!$this->inputFilter) {
    	    $inputFilter = new InputFilter();
    	    
    	    $inputFilter->add(array(
    	    		'name'     => 'username',
    	    		'required' => true,
    	    		'filters'  => array(
    	    				array('name' => 'StringTrim'),
    	    		),
    	    ));
    	    $this->inputFilter = $inputFilter;
    	    }
    	   return $this->inputFilter;
	}

    
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
<?php
namespace Restmusic\Form;

use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;

class RestmusicForm extends Form
{
    protected $dbAdapter;
    
    public function __construct(AdapterInterface $dbAdapter)
    {
        parent::__construct();
        $this->dbAdapter = $dbAdapter;
        
        $this->add(array(
            'name' => 'id',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        $this->add(array(
            'name' => 'fullname',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
    }
}
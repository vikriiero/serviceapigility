<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Restmusic for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Restmusic\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Restmusic\Model\RestmusicTable;
use Restmusic\Model\Restmusic;

class IndexController extends AbstractRestfulController
{
    protected $restmusicTable;
    
    protected function methodNotAllowed()
    {
        $this->response->setSetStatus(405);
        throw new \Exception('Method Not Allowed');
    }
    
    public function get($id)
    {
        
        $village = $this->getRestmusicTable()->getRestmusic($id);
        return new JsonModel(array(
        
            'data' => $village
        ));
    }
    public function getList()
    {
        $dataList = $this->getRestmusicTable()->getList();
        die('data list sudah ada');
        return new JsonModel(array(
            'data' => $dataList->toArray()
        ));
    }
    
    
    function getRestmusicTable()
    {
        if(! $this->restmusicTable){
            $sm = $this->getServiceLocator();
            $this->restmusicTable = $sm->get('Restmusic\Model\RestmusicTable');
        }
        return $this->restmusicTable;
    }
    public function indexAction()
    {
        return array();
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /index/index/foo
        return array();
    }
}

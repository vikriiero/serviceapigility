<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Restmusic\Controller\Index' => 'Restmusic\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'restmusic' => array(
                    'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/restmusic[:id]',
                            'constraints' => array(
                                'id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Restmusic\Controller\Index',
                            ),
                        ),
                    ),
                ),
            ),
    
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        )
    )
);

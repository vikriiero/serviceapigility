<?php
namespace Restservicelog\Form;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Form\Form;

class RestservicelogForm extends Form
{
    protected $dbAdapter;
    
 public function __construct(AdapterInterface $dbAdapter)
    {
        
        // we want to ignore the name passed
        // parent::__construct(AdapterInterface $dbAdapter);
        parent::__construct();
        
        $this->setAttribute('class', 'form-horizontal');
        
        $this->dbAdapter = $dbAdapter;
       
        
        $this->add(array(
            'name' => 'id',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'id'
            )
        ));
        
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'username' 
            )
        ));
        
        
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'password'
            )
        ));
        
        $this->add(array(
            'name' => 'Submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Submit',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            )
            
        ));
    }

    public function getListUsername()
    {
        $dbAdapter = $this->dbAdapter;
        $sql = 'SELECT from user';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        
        $selectData = array();
        
        foreach ($result as $res) {
			$selectData[$res['id']] = $res['username'];
		}
		return $selectData;
	}
	
}
    
<?php
namespace Restservicelog\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;

class Restservicelog implements InputFilterAwareInterface
{
    public $id;
    public  $username;
    public  $password;
    
    protected $inputFilter;
    
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
     
    public function setPassword($clear_password)
    {
        $this->password = md5($clear_password);
    }
    
    
    
    public function exchangeArray($data){
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->username     = (!empty($data['username'])) ? $data['username'] : null;
        $this->password     = (!empty($data['password'])) ? $data['password'] : null;
    
        if(isset($data['password'])){
            $this->setPassword($data["password"]);
        }
    }
    
    
    public function getInputFilter(){
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            	
            $inputFilter->add(array(
                'name'     => 'username',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    }
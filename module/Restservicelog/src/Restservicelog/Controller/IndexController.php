<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Restservicelog for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Restservicelog\Controller;

use Restservicelog\Form\RestservicelogForm;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Restservicelog\Model\Restservicelog;
use Zend\Config\Reader\Json;

class IndexController extends AbstractRestfulController
{
//     protected $restservicelogTable;
    protected $authservice;
    
    protected function methodNotAllowed()
    {
        $this->response->setStatusCode(405);
        throw new \Exception('Method Not Allowed Gan');
    }
    
    
    
    public function get($id)
    {
        $village = $this->getRestservicelogTable()->getRestservicelog($id);
        return new JsonModel(array(
            'data' => $village
        ));
    }
    
    public function getList()
    {
        $dataList = $this->getRestservicelogTable()->getList();
        //         print_r($dataList);die;
    
        return new JsonModel(array(
            'data' => $dataList->toArray()
        ));
    }
    
    public function create($data)
    {
//         print_r($data);die;
      $request = $this->getRequest();
//                 print_r($request);die;
        if ($request->isPost()){
            $request->getPost();
                //check authentication...
                $this->getAuthService()->getAdapter()
                ->setIdentity($request->getPost('username'))
                ->setCredential($request->getPost('password'));
                
                $result = $this->getAuthService()->authenticate();
                if($result->isValid()){
                    return new JsonModel(array(
                        'data' => 'sucess login'
                    ));
//                     print_r($result);die;
// //                     return new JsonModel($result);
                }else{
//                     print_r($r)
                    return new JsonModel(array(
                        'data' => 'gagal login'
                    ));
                }
 
        }  
    }
    
    
    public function update($id,$data){
    
    
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $village = $this->getRestserviceTable()->getRestservice($id);
    
        $data['id'] = $village->id;
         
         
        $village->id = $village->id;
         
        $formVillage = new RestservicelogForm($dbAdapter);
        $formVillage->bind($village);
        $formVillage->setInputFilter($formVillage->getInputFilter());
        $formVillage->setData($data);
    
    
        if ($formVillage->isValid()) {
    
    
            try {
                $res = 	$this->getRestservicelogTable()->updateRestservicelog($data);
    
    
            }catch(\Exception $err){
    
                die('asds');
                return new JsonModel(array(
            		    		'data' => $err->getMessage()
                ));
                die('sdf');
            }
        }else{
            die;
            return new JsonModel(array(
                'data' => $err->getMessage()
            )); 
        }
        if($res){
             
            return new JsonModel(array(
                'data' => 'success update data'
            ));
        }else{
            return new JsonModel(array(
                'data' => 'failed update data'
            ));
        }
        die;
    }
    
    
    public function delete($id){
        $del  = $this->getRestservicelogTable()->deleteRestservicelog($id);
        if($del){
    
            return new JsonModel(array(
                'data' => 'success delete data'
            ));
    
        }else {
            return new JsonModel(array(
                'data' => 'failed delete data'
            ));
        }
    
    
    }
    
    function getRestservicelogTable()
    {
        if (! $this->restservicelogTable) {
            $sm = $this->getServiceLocator();
            $this->restservicelogTable = $sm->get('Restservicelog\Model\RestservicelogTable');
        }
        return $this->restservicelogTable;
    }
    
    public function authenticateAction($data)
    {
        die('hahah');
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $formRestservice = new RestservicelogForm($dbAdapter);
        
        
        $id = '';
        $role = new Restservicelog();
        $formRestservice->setInputFilter($role->getInputFilter());
        $formRestservice->setData($data);
        
        if ($formRestservice->isValid()) {
            $role->exchangeArray($formRestservice->getData());
            $id = $this->getRestservicelogTable()->saveRestservicelog($role);
            return new JsonModel(array(
                'data' => 'success insert data'
            ));
        } else {
        
            $err = $formRestservice->getMessages();
            return new JsonModel(array(
                'data' => 'canot insert data'
            ));
        }
        $form       = $this->getForm();
        $redirect = 'login';
         
        $request = $this->getRequest();
        if ($request->isPost()){
            $form->setData($request->getPost());
            if ($form->isValid()){
                //check authentication...
                $this->getAuthService()->getAdapter()
                ->setIdentity($request->getPost('username'))
                ->setCredential($request->getPost('password'));
    
                $result = $this->getAuthService()->authenticate();
                //                 print_r($result);die;
                foreach($result->getMessages() as $message)
                {
                    //save message temporary into flashmessenger
                    $this->flashmessenger()->addMessage($message);
                }
                 
                if ($result->isValid()) {
                    //                     print_r($result);die;
                    $redirect = 'success';
                    //check if it has rememberMe :
                    if ($request->getPost('rememberme') == 1 ) {
                        $this->getSessionStorage()
                        ->setRememberMe(1);
                        //set storage again
                        $this->getAuthService()->setStorage($this->getSessionStorage());
                    }
                    $this->getAuthService()->getStorage()->write($request->getPost('username'));
                    return new JsonModel(array(
                        'data' => 'succes login'
                    ));
                } else {
    
                    $err = $result->getMessages();
                    return new JsonModel(array(
                        'data' => ' Supplied credential is invalid.'
                    ));
                }
            }
        }
        //         return new JsonModel(array(
        //             'data' => $form
        //         ));
         
        return $this->redirect()->toRoute($redirect);
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
         
        return $this->authservice;
    }
}

<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Restservicelog\Controller\Index' => 'Restservicelog\Controller\IndexController',
        ),
    ),
   'router' => array(
        'routes' => array(
            'restservicelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/restservicelog[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    )
                    ,
                    'defaults' => array(
                        'controller' => 'Restservicelog\Controller\Index'
                    )
                )
            )
        )
    ),
    
    'view_manager' => array(
        
        'strategies' => array(
            'ViewJsonStrategy'
        )
    )
    
)
;